const swig = require('swig-templates');

swig.setFilter('selectattr', function(input, attr, relation, value) {
	var params = {
		attr: attr,
		rel: relation,
		val: value
	};
	return input.filter(function(item) {
		var actual = item[params.attr];
		var expected = params.val;
		var rel = params.rel;
		if (rel == '=') {
			return (actual == expected);
		} else if (rel == '!=') {
			return !(actual == expected);
		} else {
			return true;
		}
	});
});

swig.setFilter('sortby', function(input, k) {
	input.data.sort(function (a, b) {
		return a[k] - b[k];
	});
	return input;
});
